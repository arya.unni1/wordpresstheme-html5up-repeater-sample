<?php get_header(); ?>

<?php
if(have_posts()):
 while(have_posts()):the_post();
 

$contentPart = get_field('content_parts');

?>

      <!-- One -->
	  <section id="one">
          
		  <div class="image main" data-position="center">
		  
<img src="<?php 
echo the_field('header_image') ?>" alt="" />
		  </div>

				<div class="container">
					<header class="major">
						<h2><?php echo $contentPart['title'] ?></h2>
						<p><?php echo $contentPart['subtitle'] ?></p>
					</header>
					<p><?php echo $contentPart['titledescription'] ?></p>
				</div>
			</section>

		<!-- Two -->				
			<section id="two">
				<div class="container">
					<h3><?php echo $contentPart['sectiontwo-title'] ?></h3>
					<p><?php echo $contentPart['sectiontwo-description'] ?></p>
					<ul class="feature-icons">
					<?php
						if( have_rows('font_awesome_icon_field_repeater') ):						
						while ( have_rows('font_awesome_icon_field_repeater') ) : the_row(); ?>							
						
						<li class="icon solid <?php echo the_sub_field('font_awesome_icon') ?>">							
						
						<?php echo the_sub_field('description') ?></li>

					<?php
						endwhile;
						else :						
						endif;?>									
					</ul>
				</div>
			</section>

		<!-- Three -->
			<section id="three">
				<div class="container">
					<h3><?php echo $contentPart['sectionthree-title'] ?></h3>
					<p><?php echo $contentPart['sectionthree-description'] ?></p>
					<div class="features">
						
	<?php
		if( have_rows('accomplishments-repeater') ):							
		while ( have_rows('accomplishments-repeater') ) : the_row(); ?>
						<article>
							<a href="#" class="image"><img src=
							"<?php echo the_sub_field('accomplishments-image') ?>"
								alt="" /></a>
							<div class="inner">
								<h4><?php echo the_sub_field('accomplishments-heading') ?></h4>
								<p><?php echo the_sub_field('accomplishments-description') ?></p>
							</div>
						</article>
						<?php
		endwhile;
		else :
		// no rows found
		endif;
	?>									
					</div>
				</div>
			</section>

		<!-- Four -->
			<section id="four">
				<div class="container">
					<h3><?php echo $contentPart['sectionfour-title'] ?></h3>
					<p><?php echo $contentPart['sectionfour-description'] ?></p>
					<?php echo the_field('contactme_form') ?>
				</div>
			</section>

<!-- Footer -->


	<?php  endwhile;
	  endif;
	  ?>
</div>
<?php get_footer(); ?>


