<?php
function bootstrap_theme_assets() {
	    wp_enqueue_style( 'style', get_stylesheet_uri() );
        wp_enqueue_style( 'bootstrap_style', get_bloginfo( 'template_directory' ).'/assets/css/main.css' );
  
        wp_enqueue_script( 'jquery_script', get_bloginfo( 'template_directory' ).'/assets/js/jquery.min.js' );
        wp_enqueue_script( 'jquery_scrollex', get_bloginfo( 'template_directory' ).'/assets/js/jquery.scrollex.min.js' );
        wp_enqueue_script( 'jquery_scrolly', get_bloginfo( 'template_directory' ).'assets/js/jquery.scrolly.min.js' );
        wp_enqueue_script( 'jquery_browser', get_bloginfo( 'template_directory' ).'/assets/js/browser.min.js' );
        wp_enqueue_script( 'jquery_breakpoints', get_bloginfo( 'template_directory' ).'/assets/js/breakpoints.min.js' );
        wp_enqueue_script( 'main', get_bloginfo( 'template_directory' ).'/assets/js/main.js' );
        wp_enqueue_script( 'jquery_util', get_bloginfo( 'template_directory' ).'/assets/js/util.js' );
        
        
}

     
        
add_action( 'wp_enqueue_scripts', 'bootstrap_theme_assets');

function theme_setup(){
add_theme_support('menus');
}
add_action('after_setup_theme','theme_setup');

register_nav_menus(

    array(
        'side-menu' => __('Sidebar Menu','theme'),
    )
);
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'User-social details',
		'menu_title'	=> 'User-social details',
		'menu_slug' 	=> 'user-social-details',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	
	
}
?>